import React from 'react'
import logo from '../../resources/img/logo (1).png'
import './style.scss'

const HeaderLogo = () => {
    return (
        <div className="logo">
            <img src={logo} className="App-logo" alt="Chat logo"/>
        </div>
    )
}

export default HeaderLogo;
