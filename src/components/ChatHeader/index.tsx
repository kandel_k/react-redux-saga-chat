import React from 'react'
import PropTypes, {InferProps} from 'prop-types'
import {Icon} from 'semantic-ui-react'
import './style.scss'
import Moment from "react-moment";
import {connect} from "react-redux";

const ChatHeader: any = ({ title, usersCount, messages }: InferProps<typeof ChatHeader.propTypes>
    ) => {

    return (
        <div className='room_header'>
            <div className='room_info'>
                <p className='title'>{title}</p>
                <p><Icon name='users' /> {usersCount}</p>
                <p><Icon name='envelope outline' /> {messages.length}</p>
            </div>
            {messages && <p className='room_last-message'>Last sent <Moment fromNow>
                {messages.length > 0 && messages[messages.length - 1].createdAt}</Moment>
            </p>}
        </div>
    );
}

ChatHeader.propTypes = {
    title: PropTypes.string.isRequired,
    messages: PropTypes.array.isRequired,
    usersCount: PropTypes.number.isRequired
}

const mapStateToProps = (state: any) => ({
    title: state.chat.title,
    usersCount: state.users.users.length,
    messages: state.chat.messages
})

export default connect(
    mapStateToProps,
    null
)(ChatHeader);
