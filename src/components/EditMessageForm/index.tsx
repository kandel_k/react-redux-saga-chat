import React, {useEffect, useState} from "react";
import PropTypes, {InferType} from 'prop-types'
import {connect} from "react-redux";
import {Button, Dimmer, Form, Grid, Header, Loader, Segment} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import {deleteMessage, editMessage, fetchMessage, messageCancelEdit} from "../../actions/ChatActions";
import './style.scss';
import {history} from "../../store";
import {Redirect} from "react-router";
import ErrorMessage from "../Error";

const EditMessageForm: any = (
    {
        message,
        editMessage,
        deleteMessage,
        match,
        fetchMessage,
        messageCancelEdit,
        isLoading,
        isAuthorized
    }: InferType<typeof EditMessageForm.propTypes>
) => {
    const [text, setText] = useState('Hello');
    const [isFetchLoading, setIsFetchLoading] = useState(true);

    const onChange = (event: any) => {
        setText(event.target.value);
    }

    const onCancel = () => {
        setText(message.text);
        messageCancelEdit();
        history.push('/');
    }

    const onEdit = () => {
        if (text.length === 0) {
            deleteMessage(message.id);
            onCancel();
        } else if (text === message.text) {
            return;
        } else {
            message.text = text;
            editMessage(message);
        }
    }

    const handleKeyDown = (event: any) => {
        if (event.key === 'Enter') {
            onEdit();
            event.preventDefault();
        }
    }

    useEffect(() => {
        if (!message.id) {
            fetchMessage(match.params.id);
        } else {
            setText(message.text);
            setIsFetchLoading(false);
        }
    }, [message])

    const showForm = () => (
        <div className={'wrapper'}>
            <Grid textAlign='center' style={{height: '500px'}} verticalAlign='middle'>
                <Grid.Column style={{maxWidth: 600}}>
                    <Header style={{display: 'flex', justifyContent: 'center', fontWeight: 'normal'}}>
                        Edit message
                    </Header>
                    <Form >
                        <Segment>
                            <Form.TextArea rows={10} onChange={onChange} value={text} onKeyDown={handleKeyDown}/>
                            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                                <Button size={"large"} onClick={onCancel}>Cancel</Button>
                                <Button color='blue' size='large' disabled={message.text === text} loading={isLoading} onClick={onEdit}>
                                    Send
                                </Button>
                            </div>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
            <ErrorMessage/>
        </div>
    )

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    return (
        !isAuthorized
            ? <Redirect to={'/login'} />
            : (isFetchLoading ? showLoading() : showForm())
    );
}

EditMessageForm.propTypes = {
    message: PropTypes.object.isRequired,
    editMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    match: PropTypes.object,
    fetchMessage: PropTypes.func.isRequired,
    messageCancelEdit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isAuthorized: PropTypes.bool.isRequired
};

const mapStateToProps = (state: any) => ({
    isShow: state.chat.isShow,
    message: state.chat.fetchedMessage,
    isLoading: state.chat.isLoading,
    isAuthorized: state.users.isAuthorized
});

const actions = {
    editMessage,
    deleteMessage,
    fetchMessage,
    messageCancelEdit
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditMessageForm);
