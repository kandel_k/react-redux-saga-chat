import React, {useState} from "react";
import PropTypes, {InferProps} from 'prop-types';
import {Button, Form, FormGroup, TextArea} from "semantic-ui-react";
import './style.scss'
import {MessageInterface} from "../../containers/Chat";
import {connect} from "react-redux";
import {sendMessage} from "../../actions/ChatActions";
import {bindActionCreators} from "redux";

const MessageForm: any = ({sendMessage, currUser, isMessageSending}: InferProps<typeof MessageForm.propTypes>) => {
    const [text, setText] = useState('');

    const send = () => {
        if (text.length === 0) {
            return;
        }

        let message: MessageInterface = {
            userId: currUser.userId,
            text: text
        }

        sendMessage(message);
        setText('');
    }

    const handleChange = (event: any) => setText(event.target.value);

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.key === 'Enter') {
            send();
            event.preventDefault();
        }
    }

    return (
        <Form className={'message-form'}>
            <FormGroup>
                <Form.Field control={TextArea} placeholder={'Type your message to send'} width={12} rows={2}
                            value={text} onChange={handleChange} onKeyDown={handleKeyDown}/>
                <Button color={"blue"} size={"medium"} loading={isMessageSending} onClick={send}>Send</Button>
            </FormGroup>
        </Form>
    );
}

MessageForm.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    currUser: PropTypes.object.isRequired
}

const mapStateToProps = (state: any) => ({
    currUser: state.users.currUser,
    isMessageSending: state.chat.isMessageSending
})

const actions = {
    sendMessage
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageForm);
