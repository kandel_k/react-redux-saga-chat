import React, {useEffect, useState} from "react";
import PropTypes, {InferType} from 'prop-types'
import {connect} from "react-redux";
import {Button, Dimmer, Form, Grid, Header, Loader, Segment} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import './style.scss';
import {history} from "../../store";
import {Redirect} from "react-router";
import {addUser, editUser, fetchUser, userCancelEdit} from "../../actions/UserActions";
import {getDefaultUser} from "../../config/vars";
import ErrorMessage from "../Error";

const EditUserForm: any = (
    {
        editUser,
        match,
        user,
        userCancelEdit,
        isLoading,
        isAuthorized,
        currUser,
        addUser,
        fetchUser
    }: InferType<typeof EditUserForm.propTypes>
) => {
    const [isFetchLoading, setIsFetchLoading] = useState(true);
    const [localUser, setLocalUser] = useState(getDefaultUser);

    const onCancel = () => {
        userCancelEdit();
        history.push('/users');
    }

    const onSave = () => {
        if (match.params.id) {
            editUser(localUser);
        } else {
            addUser(localUser);
        }
    }

    useEffect(() => {
        if (match.params.id) {
            if (!user.userId) {
                fetchUser(match.params.id);
            } else {
                setLocalUser(user);
                setIsFetchLoading(false);
            }
        } else {
            setIsFetchLoading(false);
        }
    }, [user])

    const showForm = () => (
        <div>
            <Grid textAlign='center' style={{height: '500px'}} verticalAlign='middle' className={'edit-user-form'}>
                <Grid.Column style={{maxWidth: 600}}>
                    <Header style={{display: 'flex', justifyContent: 'center', fontWeight: 'normal'}}>
                        User form
                    </Header>
                    <Form >
                        <Segment >
                            <Form.Input label={"Username"} labelPosition={"left corner"} value={localUser.user}
                                        onChange={(event => setLocalUser({...localUser, user: event.target.value}))}/>

                            <Form.Input label={"Avatar"} labelPosition={"left corner"} value={localUser.avatar}
                                        onChange={(event => setLocalUser({...localUser, avatar: event.target.value}))}/>

                            <Form.Input label={"Password"} labelPosition={"left corner"} value={localUser.password}
                                        onChange={(event => setLocalUser({...localUser, password: event.target.value}))}/>

                            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                                <Button size={"large"} onClick={onCancel}>Cancel</Button>
                                <Button color='blue' size='large' loading={isLoading} onClick={onSave}>
                                    Send
                                </Button>
                            </div>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
            <ErrorMessage/>
        </div>
    )

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    return (
        (!isAuthorized || currUser.role !== 'ADMIN')
            ? <Redirect to={'/login'} />
            : (isFetchLoading ? showLoading() : showForm())
    );
}

EditUserForm.propTypes = {
    user: PropTypes.object.isRequired,
    editUser: PropTypes.func.isRequired,
    fetchUser: PropTypes.func.isRequired,
    match: PropTypes.object,
    userCancelEdit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isAuthorized: PropTypes.bool.isRequired,
    addUser: PropTypes.func.isRequired
};

const mapStateToProps = (state: any) => ({
    isLoading: state.chat.isLoading,
    isAuthorized: state.users.isAuthorized,
    currUser: state.users.currUser,
    user: state.users.fetchedUser
});

const actions = {
    editUser,
    fetchUser,
    userCancelEdit,
    addUser
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditUserForm);
