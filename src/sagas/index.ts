import {all} from 'redux-saga/effects'
import messageSagas from "./MessageSagas";
import userSagas from "./UserSagas";

export default function* rootSaga() {
    yield all([
        messageSagas(),
        userSagas()
    ]);
}
